#ifndef myUtils__h
#define myUtils__h

void log_message2(cJSON *msg, int t);
void log_array(cJSON *msg, int t) {
    cJSON *c;
    char tmpSrt[128];
    int i;
    
    //Put some spaces in the begginning.
    if (t>=128) {
        t = 127;
    }
    for(i=0; i<t; i++) {
        tmpSrt[i] = ' ';
    }
    tmpSrt[i] = '\0';
    
    for (c=msg->child; c; c=c->next) {
        if (c->type == cJSON_NULL) {
            printf("%s(NULL)\n", tmpSrt);
        } else if (c->type == cJSON_Number) {
            printf("%s%f (NUMBER)\n", tmpSrt, c->valuedouble);
        } else if (c->type == cJSON_String) {
            printf("%s%s (STRING)\n", tmpSrt, c->valuestring);
        } else if (c->type == cJSON_Array) {
            printf("%s(ARRAY)\n", tmpSrt);
            log_array(c, t+1);
        } else if (c->type == cJSON_Object) {
            printf("%s(OBJECT)\n", tmpSrt);
            log_message2(c, t+1);
        } else {
            printf("%s(%d)\n", tmpSrt, c->type);
        }
    }
}

void log_message2(cJSON *msg, int t) {
    cJSON *c;
    char tmpSrt[128];
    int i;
    
    //Put some spaces in the begginning.
    if (t>=128) {
        t = 127;
    }
    for(i=0; i<t; i++) {
        tmpSrt[i] = ' ';
    }
    tmpSrt[i] = '\0';
    
    for (c=msg->child; c; c=c->next) {
        if (c->type == cJSON_NULL) {
            printf("%s%s (NULL)\n", tmpSrt, c->string);
        } else if (c->type == cJSON_Number) {
            printf("%s%s: %f (NUMBER)\n", tmpSrt, c->string, c->valuedouble);
        } else if (c->type == cJSON_String) {
            printf("%s%s: %s (STRING)\n", tmpSrt, c->string, c->valuestring);
        } else if (c->type == cJSON_Array) {
            printf("%s%s: (ARRAY)\n", tmpSrt, c->string);
            log_array(c, t+1);
        } else if (c->type == cJSON_Object) {
            printf("%s%s: (OBJECT)\n", tmpSrt, c->string);
            log_message2(c, t+1);
        } else {
            printf("%s%s (%d)\n", tmpSrt, c->string, c->type);
        }
    }
}

#endif
