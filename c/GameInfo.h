#ifndef GameInfo__h
#define GameInfo__h

#define S 128
#define L 4

#define StraightTrackPiece 0
#define BendTrackPiece 1

#define BestPositionLeft -1
#define BestPositionRight 1
#define BestPositionBoth 3
#define BestPositionUnknown 0

struct Car {
    char name[S];
    char color[S];
    
    double length;
    double width;
    double guideFlagPosition;
    
    double angle;
    int pieceIndex;
    double inPieceDistance;
    int startLaneIndex;
    int endLaneIndex;
    int lap;
};
typedef struct Car Car;

struct TrackPiece {
    int type;
    
    double length;  //Used with type==StraightTrackPiece
    
    double angle;       //Used with type==BendTrackPiece
    double radius;      //Used with type==BendTrackPiece
    double leftLength;  //Used with type==BendTrackPiece
    double rightLength; //Used with type==BendTrackPiece
    
    int bestPosition;   //Which is the best lane.
    
    int isSwitch;     //1 = true
};
typedef struct TrackPiece TrackPiece;

struct GameInfo {
    char teamName[S];
    char teamColor[S];
    double throttle;
    
    TrackPiece *pieces;
    int piecesNumber;
    int piecesNumberMax;
    
    double lanesDistanceFromCenter[L];
    int    lanesNumber;
    
    Car *cars;
    int carsNumber;
    int carsNumberMax;
    
    int laps;
    
    int gameTick;
};
typedef struct GameInfo GameInfo;

GameInfo *createGameInfo() {
    GameInfo *gameInfo;
    
    gameInfo = (GameInfo*) (malloc(sizeof(GameInfo)));
    if (gameInfo==NULL) {
        printf("Can not alloc gameInfo\n");
        exit(1);
    }
    strncpy(gameInfo->teamName, "", S);
    strncpy(gameInfo->teamColor, "", S);
    gameInfo->throttle = 0;
    gameInfo->lanesNumber = 0;
    gameInfo->gameTick = -1;
    gameInfo->laps = 0;
    
    //Initialize an number of pieces.
    gameInfo->piecesNumberMax = 1024;
    gameInfo->piecesNumber = 0;
    gameInfo->pieces = (TrackPiece*) (malloc(gameInfo->piecesNumberMax * sizeof(TrackPiece)));
    
    //Initialize number of cars.
    gameInfo->carsNumberMax = 1024;
    gameInfo->carsNumber = 0;
    gameInfo->cars = (Car*) (malloc(gameInfo->carsNumberMax * sizeof(Car)));
    
    return gameInfo;
}

void deleteGameInfo(GameInfo *gameInfo) {
    free(gameInfo->pieces);
    free(gameInfo->cars);
    free(gameInfo);
}

void printGameInfo(GameInfo *gameInfo) {
    int i;
    
    printf("Game Info:\n");
    printf("TeamName: %s\n", gameInfo->teamName);
    printf("TeamColor: %s\n", gameInfo->teamColor);
    for (i=0; i<gameInfo->piecesNumber; i++) {
        TrackPiece *piece = &(gameInfo->pieces[i]);
        char bestPostion[S];
        if (piece->bestPosition==BestPositionLeft) {
            strncpy(bestPostion, "BestPositionLeft", S);
        } else if (piece->bestPosition==BestPositionRight) {
            strncpy(bestPostion, "BestPositionRight", S);
        } else if (piece->bestPosition==BestPositionBoth) {
            strncpy(bestPostion, "BestPositionBoth", S);
        } else {
            strncpy(bestPostion, "BestPositionUnknown", S);
        }
        if (piece->type==StraightTrackPiece) {
            if (piece->isSwitch) {
                printf("Piece[%d] Straight Switch: %.2f, %s\n", i, piece->length, bestPostion);
            } else {
                printf("Piece[%d] Straight: len=%.2f, %s\n", i, piece->length, bestPostion);
            }
        } else {
            if (piece->isSwitch) {
                printf("Piece[%d] Bend Switch: rad=%.2f, ang=%.2f, left=%.2f, right=%.2f, %s\n", i, piece->radius, piece->angle, piece->leftLength, piece->rightLength, bestPostion);
            } else {
                printf("Piece[%d] Bend: rad=%.2f ang=%.2f, left=%.2f, right=%.2f, %s\n", i, piece->radius, piece->angle, piece->leftLength, piece->rightLength, bestPostion);
            }
        }
    }
    for (i=0; i<gameInfo->lanesNumber; i++) {
        printf("Lane[%d] distance: %.2f\n", i, gameInfo->lanesDistanceFromCenter[i]);
    }
    for (i=0; i<gameInfo->carsNumber; i++) {
        Car *car = &(gameInfo->cars[i]);
        printf("Car[%d]: name=%s, color=%s, length=%.2f, width=%.2f, guideFlagPosition=%.2f, angle=%.2f, pieceIndex=%d, inPieceDistance=%f, startLaneIndex=%d, endLaneIndex=%d, lap=%d\n", i, car->name, car->color, car->length, car->width, car->guideFlagPosition, car->angle, car->pieceIndex, car->inPieceDistance, car->startLaneIndex, car->endLaneIndex, car->lap);
    }
    printf("Laps: %d\n", gameInfo->laps);
    printf("GameTick: %d\n", gameInfo->gameTick);
}

double arcLength(double rarius, double angle) {
    return (angle/360.0) * 2 * 3.14159265359 * rarius;
}

void calculateBendPiecesLengthBetweenIndexes(GameInfo *gameInfo, int firstIndex, int afterLastIndex, double *leftLanesLength, double *rightLanesLength) {
    int i;
    *leftLanesLength = 0;
    *rightLanesLength = 0;
    for (i=firstIndex; i<afterLastIndex; i++) {
        TrackPiece *piece = &(gameInfo->pieces[i]);
        if (piece->type==BendTrackPiece) {
            (*leftLanesLength) += piece->leftLength;
            (*rightLanesLength) += piece->rightLength;
        }
    }
}

int myCarIndex(GameInfo *gameInfo) {
    int i;
    for (i=0; i<gameInfo->carsNumber; i++) {
        Car *car = &(gameInfo->cars[i]);
        if (!strncmp(gameInfo->teamName, car->name, S) && !strncmp(gameInfo->teamColor, car->color, S)) {
            return i;
        }
    }
    return -1;
}

void increasePiecesArrayIfNeeded(GameInfo *gameInfo) {
    if (gameInfo->piecesNumber == gameInfo->piecesNumberMax) {
        gameInfo->piecesNumberMax += 1024;
        gameInfo->pieces = (TrackPiece*) (realloc(gameInfo->pieces, gameInfo->piecesNumberMax * sizeof(TrackPiece)));
    }
}

void addStraightPiece(GameInfo *gameInfo, double length, int isSwitch) {
    increasePiecesArrayIfNeeded(gameInfo);
    gameInfo->pieces[gameInfo->piecesNumber].type = StraightTrackPiece;
    gameInfo->pieces[gameInfo->piecesNumber].length = length;
    gameInfo->pieces[gameInfo->piecesNumber].isSwitch = isSwitch;
    gameInfo->pieces[gameInfo->piecesNumber].bestPosition = BestPositionUnknown;
    gameInfo->piecesNumber++;
}

void addBendPiece(GameInfo *gameInfo, double radius, double angle, int isSwitch) {
    increasePiecesArrayIfNeeded(gameInfo);
    gameInfo->pieces[gameInfo->piecesNumber].type = BendTrackPiece;
    gameInfo->pieces[gameInfo->piecesNumber].radius = radius;
    gameInfo->pieces[gameInfo->piecesNumber].angle = angle;
    gameInfo->pieces[gameInfo->piecesNumber].isSwitch = isSwitch;
    gameInfo->pieces[gameInfo->piecesNumber].bestPosition = BestPositionUnknown;
    gameInfo->pieces[gameInfo->piecesNumber].leftLength = 0;
    gameInfo->pieces[gameInfo->piecesNumber].rightLength = 0;
    gameInfo->piecesNumber++;
}

void yourCarJson(GameInfo *gameInfo, cJSON *json) {
    cJSON *dataJson = cJSON_GetObjectItem(json, "data");
    cJSON *nameJson = cJSON_GetObjectItem(dataJson, "name");
    cJSON *colorJson = cJSON_GetObjectItem(dataJson, "color");
    strncpy(gameInfo->teamName, nameJson->valuestring, S);
    strncpy(gameInfo->teamColor, colorJson->valuestring, S);
}

void gameStartJson(GameInfo *gameInfo, cJSON *json) {
    cJSON *gameTickJson = cJSON_GetObjectItem(json, "gameTick");
    
    //printf("Inside gameStartJson\n");
    if (gameTickJson) {
        //printf("inside if\n");
        gameInfo->gameTick = gameTickJson->valueint;
    }
}

void gameInitJson(GameInfo *gameInfo, cJSON *json) {
    cJSON *c;
    
    cJSON *dataJson = cJSON_GetObjectItem(json, "data");
    cJSON *raceJson = cJSON_GetObjectItem(dataJson, "race");
    
    cJSON *trackJson = cJSON_GetObjectItem(raceJson, "track");
    cJSON *carsArray = cJSON_GetObjectItem(raceJson, "cars");
    cJSON *raceSessionJson = cJSON_GetObjectItem(raceJson, "raceSession");
    
    cJSON *piecesArray = cJSON_GetObjectItem(trackJson, "pieces");
    cJSON *lanesArray = cJSON_GetObjectItem(trackJson, "lanes");
    
    //Pieces.
    for (c=piecesArray->child; c; c=c->next) {
        //Check if it is switch.
        int isSwitch = 0;
        cJSON *switchJson = cJSON_GetObjectItem(c, "switch");
        if (switchJson) {
            if (switchJson->type==cJSON_True) {
                isSwitch = 1;
            }
        }
        
        //Check if it is straight or an angle
        cJSON *lengthJson = cJSON_GetObjectItem(c, "length");
        if (lengthJson) {
            //Setted: length.
            double length = lengthJson->valuedouble;
            addStraightPiece(gameInfo, length, isSwitch);
        } else {
            cJSON *radiusJson = cJSON_GetObjectItem(c, "radius");
            cJSON *angleJson = cJSON_GetObjectItem(c, "angle");
            if (radiusJson && angleJson) {
                //Setted: radius, angle.
                double radius = radiusJson->valuedouble;
                double angle = angleJson->valuedouble;
                addBendPiece(gameInfo, radius, angle, isSwitch);
            }
        }
    }
    
    //Lanes
    for (c=lanesArray->child; c; c=c->next) {
        cJSON *distanceFromCenterJson = cJSON_GetObjectItem(c, "distanceFromCenter");
        cJSON *indexJson = cJSON_GetObjectItem(c, "index");
        if (indexJson->valueint < L) {
            gameInfo->lanesDistanceFromCenter[indexJson->valueint] = distanceFromCenterJson->valuedouble;
            gameInfo->lanesNumber++;
        }
    }
    
    //Cars
    for (c=carsArray->child; c; c=c->next) {
        cJSON *idJson = cJSON_GetObjectItem(c, "id");
        cJSON *dimensionsJson = cJSON_GetObjectItem(c, "dimensions");
        
        if (idJson && dimensionsJson) {
            cJSON *nameJson = cJSON_GetObjectItem(idJson, "name");
            cJSON *colorJson = cJSON_GetObjectItem(idJson, "color");
            
            cJSON *lengthJson = cJSON_GetObjectItem(dimensionsJson, "length");
            cJSON *widthJson = cJSON_GetObjectItem(dimensionsJson, "width");
            cJSON *guideFlagPositionJson = cJSON_GetObjectItem(dimensionsJson, "guideFlagPosition");
            
            if (nameJson && colorJson && lengthJson && widthJson && guideFlagPositionJson) {
                //Increase the sizei if needed.
                if (gameInfo->carsNumber == gameInfo->carsNumberMax) {
                    gameInfo->carsNumberMax += 1024;
                    gameInfo->cars = (Car*) (realloc(gameInfo->cars, gameInfo->carsNumberMax * sizeof(Car)));
                }
                
                //Add the car.
                strncpy(gameInfo->cars[gameInfo->carsNumber].name, nameJson->valuestring, S);
                strncpy(gameInfo->cars[gameInfo->carsNumber].color, colorJson->valuestring, S);
                gameInfo->cars[gameInfo->carsNumber].length = lengthJson->valuedouble;
                gameInfo->cars[gameInfo->carsNumber].width = widthJson->valuedouble;
                gameInfo->cars[gameInfo->carsNumber].guideFlagPosition = guideFlagPositionJson->valuedouble;
                gameInfo->cars[gameInfo->carsNumber].angle = 0;
                gameInfo->cars[gameInfo->carsNumber].pieceIndex = 0;
                gameInfo->cars[gameInfo->carsNumber].inPieceDistance = 0;
                gameInfo->cars[gameInfo->carsNumber].startLaneIndex = 0;
                gameInfo->cars[gameInfo->carsNumber].endLaneIndex = 0;
                gameInfo->cars[gameInfo->carsNumber].lap = 0;
                gameInfo->carsNumber++;
            }
        }
    }
    
    cJSON *lapsJson = cJSON_GetObjectItem(raceSessionJson, "laps");
    if (lapsJson) {
        gameInfo->laps = lapsJson->valueint;
    }
}

void carPositionsJson(GameInfo *gameInfo, cJSON *json) {
    cJSON *c;
    cJSON *dataJson = cJSON_GetObjectItem(json, "data");
    cJSON *gameTickJson = cJSON_GetObjectItem(json, "gameTick");
    
    if (gameTickJson) {
        gameInfo->gameTick = gameTickJson->valueint;
    }
    
    for (c=dataJson->child; c; c=c->next) {
        cJSON *idJson = cJSON_GetObjectItem(c, "id");
        cJSON *angleJson = cJSON_GetObjectItem(c, "angle");
        cJSON *piecePositionJson = cJSON_GetObjectItem(c, "piecePosition");
        
        if (idJson && piecePositionJson) {
            cJSON *nameJson = cJSON_GetObjectItem(idJson, "name");
            cJSON *colorJson = cJSON_GetObjectItem(idJson, "color");
            
            cJSON *pieceIndexJson = cJSON_GetObjectItem(piecePositionJson, "pieceIndex");
            cJSON *inPieceDistanceJson = cJSON_GetObjectItem(piecePositionJson, "inPieceDistance");
            cJSON *laneJson = cJSON_GetObjectItem(piecePositionJson, "lane");
            cJSON *lapJson = cJSON_GetObjectItem(piecePositionJson, "lap");
            
            if (laneJson) {
                cJSON *startLaneIndexJson = cJSON_GetObjectItem(laneJson, "startLaneIndex");
                cJSON *endLaneIndexJson = cJSON_GetObjectItem(laneJson, "endLaneIndex");
                
                if (nameJson && colorJson && angleJson && pieceIndexJson && inPieceDistanceJson && startLaneIndexJson && endLaneIndexJson && lapJson) {
                    int i;
                    //Find the car and update info.
                    for (i=0; i<gameInfo->carsNumber; i++) {
                        if (!strncmp(gameInfo->cars[i].name, nameJson->valuestring, S) && !strncmp(gameInfo->cars[i].color, colorJson->valuestring, S)) {
                            gameInfo->cars[i].angle = angleJson->valuedouble;
                            gameInfo->cars[i].pieceIndex = pieceIndexJson->valueint;
                            gameInfo->cars[i].inPieceDistance = inPieceDistanceJson->valuedouble;
                            gameInfo->cars[i].startLaneIndex = startLaneIndexJson->valueint;
                            gameInfo->cars[i].endLaneIndex = endLaneIndexJson->valueint;
                            gameInfo->cars[i].lap = lapJson->valueint;
                            break;
                        }
                    }
                }
            }
        }
    }
}

void findBestPositions(GameInfo *gameInfo) {
    int i;
    
    int firstSwitchIndex = -1;
    int secondSwitchIndex = -1;
    //Find the first switch.
    for (i=0; i<gameInfo->piecesNumber; i++) {
        if (gameInfo->pieces[i].isSwitch) {
            firstSwitchIndex = i;
            break;
        }
    }
    if (firstSwitchIndex==-1) {
        //No switches exist.
        return;
    }
    //Find the second switch
    for (i=firstSwitchIndex+1; i<gameInfo->piecesNumber; i++) {
        if (gameInfo->pieces[i].isSwitch) {
            secondSwitchIndex = i;
            break;
        }
    }
    
    //Compute the length of bend pieces.
    for (i=0; i<gameInfo->piecesNumber; i++) {
        TrackPiece *piece = &(gameInfo->pieces[i]);
        if (piece->type==BendTrackPiece) {
            if (piece->angle>0) {
                piece->leftLength = arcLength(piece->radius - gameInfo->lanesDistanceFromCenter[0], piece->angle);
                piece->rightLength = arcLength(piece->radius - gameInfo->lanesDistanceFromCenter[gameInfo->lanesNumber-1], piece->angle);
            } else {
                piece->leftLength = arcLength(piece->radius + gameInfo->lanesDistanceFromCenter[0], -piece->angle);
                piece->rightLength = arcLength(piece->radius + gameInfo->lanesDistanceFromCenter[gameInfo->lanesNumber-1], -piece->angle);
            }
            
            
            //Just in case the lanes are been given in reverse order.
            if (gameInfo->lanesDistanceFromCenter[0] > gameInfo->lanesDistanceFromCenter[gameInfo->lanesNumber-1]) {
                double tmpD = piece->leftLength;
                piece->leftLength = piece->rightLength;
                piece->rightLength = tmpD;
            }
        }
    }
    
    if (secondSwitchIndex==-1) {
        //Only one switch exist.
        
        //Find the left and right lane length of the track. Straight lanes do not count.
        double leftLanesLength;
        double rightLanesLength;
        calculateBendPiecesLengthBetweenIndexes(gameInfo, 0, gameInfo->piecesNumber, &leftLanesLength, &rightLanesLength);
        int bestPosition = BestPositionBoth;
        if (leftLanesLength < rightLanesLength) {
            bestPosition = BestPositionRight;
        } else if (leftLanesLength > rightLanesLength) {
            bestPosition = BestPositionLeft;
        }
        for (i=0; i<gameInfo->piecesNumber; i++) {
            TrackPiece *piece = &(gameInfo->pieces[i]);
            piece->bestPosition = bestPosition;
        }
        
    } else {
        //At least two switches exist. Change the best position of the pieces between the two switches.
        double leftLanesLength, rightLanesLength, leftLanesLength2, rightLanesLength2;
        int bestPosition;
        
        while (secondSwitchIndex<gameInfo->piecesNumber) {
            calculateBendPiecesLengthBetweenIndexes(gameInfo, firstSwitchIndex, secondSwitchIndex, &leftLanesLength, &rightLanesLength);
            bestPosition = BestPositionBoth;
            if (leftLanesLength < rightLanesLength) {
                bestPosition = BestPositionLeft;
            } else if (leftLanesLength > rightLanesLength) {
                bestPosition = BestPositionRight;
            }
            for (i=firstSwitchIndex; i<secondSwitchIndex; i++) {
                TrackPiece *piece = &(gameInfo->pieces[i]);
                piece->bestPosition = bestPosition;
            }
            //Find the next switch.
            firstSwitchIndex = secondSwitchIndex;
            secondSwitchIndex++;
            while (secondSwitchIndex<gameInfo->piecesNumber) {
                if (gameInfo->pieces[secondSwitchIndex].isSwitch) {
                    break;
                }
                secondSwitchIndex++;
            }
        }
        //The firstSwitchIndex contains the last switch of the track. We must find the first one
        for (i=0; i<gameInfo->piecesNumber; i++) {
            if (gameInfo->pieces[i].isSwitch) {
                secondSwitchIndex = i;
                break;
            }
        }
        //We have two parts: firstSwitchIndex-gameInfo->piecesNumber, 0-secondSwitchIndex.
        calculateBendPiecesLengthBetweenIndexes(gameInfo, firstSwitchIndex, gameInfo->piecesNumber, &leftLanesLength, &rightLanesLength);
        calculateBendPiecesLengthBetweenIndexes(gameInfo, 0, secondSwitchIndex, &leftLanesLength2, &rightLanesLength2);
        leftLanesLength += leftLanesLength2;
        rightLanesLength += rightLanesLength2;
        bestPosition = BestPositionBoth;
        if (leftLanesLength < rightLanesLength) {
            bestPosition = BestPositionLeft;
        } else if (leftLanesLength > rightLanesLength) {
            bestPosition = BestPositionRight;
        }
        for (i=firstSwitchIndex; i<gameInfo->piecesNumber; i++) {
            TrackPiece *piece = &(gameInfo->pieces[i]);
            piece->bestPosition = bestPosition;
        }
        for (i=0; i<secondSwitchIndex; i++) {
            TrackPiece *piece = &(gameInfo->pieces[i]);
            piece->bestPosition = bestPosition;
        }
    }
}

void updateGameInfoWithJson(GameInfo *gameInfo, cJSON *json) {
    cJSON *msg_type;
    char *msg_type_name;
    
    //Get message type
    msg_type = cJSON_GetObjectItem(json, "msgType");
    if (msg_type == NULL) {
        return;
    }
    msg_type_name = msg_type->valuestring;
    
    if (!strcmp("yourCar", msg_type_name)) {
        yourCarJson(gameInfo, json);
    } else if (!strcmp("join", msg_type_name)) {
        //Need somthing?
    } else if (!strcmp("gameInit", msg_type_name)) {
        gameInitJson(gameInfo, json);
        findBestPositions(gameInfo);
    } else if (!strcmp("gameStart", msg_type_name)) {
        gameStartJson(gameInfo, json);
    } else if (!strcmp("carPositions", msg_type_name)) {
        carPositionsJson(gameInfo, json);
    }
}

#endif
