#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

#include "GameInfo.h"
#include "myUtils.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *switchLane_msg(char *movement);
static cJSON *join_msg2(char *bot_name, char *bot_key);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

/*
static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;
    
    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}
 */

GameInfo *gameInfo = NULL;

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    printf("Start program\n");
    
    json = join_msg(argv[3], argv[4]);
    //json = join_msg2(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);

    gameInfo = createGameInfo();
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL) {
            continue;
        }
        msg_type_name = msg_type->valuestring;
        
        //Get tick of messages.
        if (!strcmp("gameStart", msg_type_name) || !strcmp("carPositions", msg_type_name)) {
            cJSON *c = cJSON_GetObjectItem(json, "gameTick");
            if (c && c->valuedouble > gameInfo->gameTick) {
                gameInfo->gameTick = c->valuedouble;
            }
        }
        
        //log_message2(json, 0);
        //printf("\n");
        
        //Update game info based on message.
        updateGameInfoWithJson(gameInfo, json);
        //printGameInfo(gameInfo);
        //printf("\n");
        
        if (!strcmp("gameStart", msg_type_name)) {
            //printf("\n>>> gameStart \n");
            gameInfo->throttle = 0;
            msg = throttle_msg(1);
        } else if (!strcmp("carPositions", msg_type_name)) {
            int myCarInd = myCarIndex(gameInfo);
            if (myCarInd>=0) {
                Car *myCar = &(gameInfo->cars[myCarInd]);
                TrackPiece *pieceNow = &(gameInfo->pieces[myCar->pieceIndex]);
                TrackPiece *pieceNext =&(gameInfo->pieces[myCar->pieceIndex+1 < gameInfo->piecesNumber-1 ? myCar->pieceIndex+1 : 0]);
                pieceNow = pieceNext;
                int changeLane = 0;  //0=do not change, -1=move left, 1=move right.
                //Check if the car is in the correct lane.
                if (pieceNow->bestPosition==BestPositionLeft && myCar->startLaneIndex!=0) {
                    changeLane = -1;
                } else if (pieceNow->bestPosition==BestPositionRight && myCar->startLaneIndex!=gameInfo->lanesNumber-1) {
                    changeLane = 1;
                }
                
                //printf("pieceNow %d, changeLane:%d, pieceNext->isSwitch:%d pieceNow->bestPosition:%d\n\n", myCar->pieceIndex, changeLane, pieceNow->isSwitch, pieceNow->bestPosition);
                
                if (gameInfo->gameTick%2==0 && gameInfo->throttle>0 && changeLane!=0 && pieceNow->isSwitch) {
                    printf("\n>>> pieceIndex:%d changeLane:%d\n", myCar->pieceIndex, changeLane);
                    if (changeLane==-1) {
                        msg = switchLane_msg("Left");
                    } else {
                        msg = switchLane_msg("Right");
                    }
                } else {
                    printf("\n>>> Trottle\n");
                    gameInfo->throttle = 0.4;
                    msg = throttle_msg(gameInfo->throttle);
                }
            } else {
                msg = ping_msg();
            }
        } else {
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }
    deleteGameInfo(gameInfo);

    return 0;
}

static cJSON *make_msgWithTick(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    if (gameInfo && gameInfo->gameTick>-1) {
        cJSON_AddItemToObject(json, "gameTick", cJSON_CreateNumber(gameInfo->gameTick));
    }
    return json;
}

static cJSON *switchLane_msg(char *movement)
{
    return make_msgWithTick("switchLane", cJSON_CreateString(movement));
}

static cJSON *ping_msg()
{
    return make_msgWithTick("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *join_msg2(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", botId);
    //cJSON_AddStringToObject(data, "trackName", "keimola");
    //cJSON_AddStringToObject(data, "trackName", "germany");
    //cJSON_AddStringToObject(data, "trackName", "usa");
    cJSON_AddStringToObject(data, "trackName", "france");
    cJSON_AddNumberToObject(data, "carCount", 1);
    
    return make_msg("joinRace", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msgWithTick("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
